<?php
/**
 * @file
 * - Theme settings file.
 */

if (!defined('__DIR__')) {
  define('__DIR__', dirname(__FILE__));
}

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function cmsbots_form_system_theme_settings_alter(&$form, $form_state) {
  $path_to_theme = drupal_get_path('theme', 'cmsbots');

  // Get the theme name.
  $theme = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : FALSE;

  $homepage_regions['header_navy_blue_slider']['regionname']  = 'Header navy blue slider';
  $homepage_regions['header_navy_blue_slider']['designid']  = '144';
  $homepage_regions['content_dark_pink']['regionname']  = 'Content dark pink';
  $homepage_regions['content_dark_pink']['designid']  = '111';
  $homepage_regions['content_pink']['regionname']  = 'Content pink';
  $homepage_regions['content_pink']['designid']  = '82';
  $homepage_settings = _mbase_subtheme_themesettings($homepage_regions, $theme, 'frontpage', 'Front Page');
  $form = array_merge($form,$homepage_settings);

  $footer_regions['footer_menu']['regionname']  = 'Footer Menu';
  $footer_regions['footer_menu']['designid']  = '68';
  $footer_regions['footer_copyright']['regionname']  = 'Footer Copyright';
  $footer_regions['footer_copyright']['designid']  = '68';
  $footer_settings = _mbase_subtheme_themesettings($footer_regions, $theme, 'footer', 'Site wide Footer');
  $form = array_merge($form,$footer_settings);
}

